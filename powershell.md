### Create a file named main.txt
---
```powershell
>>New-Item main.txt
```
### Read Contents of file main.txt
---
```powershell
>>get-Content main.txt
```
### Sort items in file main.txt
```powershell
>>Get-Content main.txt | Sort-Object
```
### Get unique items from file main.txt
```powershell
>>Get-Content main.txt | Sort-Object -unique
```